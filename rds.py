import os
import sys
import boto3
import utils

from troposphere.iam import Role, InstanceProfile

from troposphere.rds import (
    DBInstance, DBCluster, DBParameterGroup,
    DBClusterParameterGroup
)

from troposphere.ec2 import (
    SecurityGroup, SecurityGroupRule
)

from troposphere import (
    GetAtt, Join, Output,
    Parameter, Ref, Template,
    GetAZs, AWS_REGION
)

cidrListStr = os.environ.get('ALLOWCIDR', '')
cidrList = cidrListStr.strip().split(',')
cidrList.append(utils.get_default_vpc_cidr())
if len(cidrList) == 0:
    print("cidr list for RDS access security group is missing, aborting")
    sys.exit(1)

t = Template()

t.add_version()

t.add_description("""AWS CloudFormation for GrowthPlay:
  - RDS Aurora MySQL 5.7
  - ECS for frontend
  - EBS for 2 backend services""")

dbuser = t.add_parameter(Parameter(
    "DBUser",
    NoEcho=True,
    Description="The database admin account username",
    Type="String",
    MinLength="1",
    MaxLength="16",
    AllowedPattern="[a-zA-Z][a-zA-Z0-9]*",
    ConstraintDescription=("must begin with a letter and contain only alphanumeric characters.")
))

dbpassword = t.add_parameter(Parameter(
    "DBPassword",
    NoEcho=True,
    Description="The database admin account password",
    Type="String",
    MinLength="1",
    MaxLength="41",
    AllowedPattern="[a-zA-Z0-9]*",
    ConstraintDescription="must contain only alphanumeric characters."
))

dbport = t.add_parameter(Parameter(
    'DBPort',
    Type='String',
    Default='3306'
))

dbname = t.add_parameter(Parameter(
    'DBName',
    Type='String',
    Default='legend'
))

sgiList=[]
for c in cidrList:
    sgiList.append(
        SecurityGroupRule(
            IpProtocol='tcp',
            FromPort=Ref(dbport),
            ToPort=Ref(dbport),
            CidrIp=c
        )
    )

sg = t.add_resource(SecurityGroup(
    'RDSSecurityGroup',
    GroupDescription='RDS security group',
    SecurityGroupIngress=sgiList
))

dbcluster = t.add_resource(DBCluster(
    'LegendAppCluster',
    AvailabilityZones=GetAZs(Ref(AWS_REGION)),
    DatabaseName=Ref(dbname),
    Engine='aurora-mysql',
    MasterUsername=Ref(dbuser),
    MasterUserPassword=Ref(dbpassword),
    Port=Ref(dbport),
    DBClusterParameterGroupName='default.aurora-mysql5.7',
    VpcSecurityGroupIds=[GetAtt(sg, 'GroupId')],
    DeletionPolicy='Delete',
))

mydb = t.add_resource(DBInstance(
    "GPLegendDB",
    DBClusterIdentifier=Ref(dbcluster),
    StorageType='aurora',
    DBInstanceClass="db.t2.small",
    Engine="aurora-mysql",
    EngineVersion="5.7",
    DBParameterGroupName='default.aurora-mysql5.7',
    PubliclyAccessible=True
))

t.add_output([
    Output(
        'Host',
        Description='Database Host to connect to',
        Value= GetAtt(mydb, 'Endpoint.Address')
    ),
    Output(
        'User',
        Value=Ref(dbuser)
    ),
    Output(
        'Password',
        Value=Ref(dbpassword)
    ),
    Output(
        'Database',
        Value=Ref(dbname)
    )
    ]
)

#print(t.to_json(sort_keys=True))

#with open('{}.json'.format(os.path.splitext(os.path.basename(__file__))[0]), 'w') as f:
#    f.write(t.to_json(sort_keys=True))



client = boto3.client('cloudformation')
response = client.create_stack(
    StackName=os.environ.get(
        'STACK_NAME',
        '{}-{}'.format(
            os.environ.get('JOB_NAME'),
            os.environ.get('BUILD_NUMBER', 0)
        )
    ),
    TemplateBody=str(t.to_json()),
    Parameters=[
        {
            'ParameterKey': 'DBUser',
            'ParameterValue': os.environ['DBUSER']
        },
        {
            'ParameterKey': 'DBPassword',
            'ParameterValue': os.environ['DBPASSWORD']
        },
        {
            'ParameterKey': 'DBPort',
            'ParameterValue': os.environ.get('DBPORT', '3306')
        },
        {
            'ParameterKey': 'DBName',
            'ParameterValue': os.environ.get('DBNAME', 'legend')
        }
    ],
    Capabilities=[]
)
if 'StackId' in response:
    print(response['StackId'])
else:
    print(response)
