import os
import boto3

def get_default_vpc_cidr():
    ec2_conn_client = boto3.client('ec2')

    r = ec2_conn_client.describe_vpcs(
        Filters=[
            {
                'Name': 'isDefault',
                'Values': ['true']
            }
        ]
    )
    try:
        return r['Vpcs'][0]['CidrBlock']
    except IndexError:
        return None 

def get_default_vpc_subnets():
    ec2_conn_client = boto3.client('ec2')

    r = ec2_conn_client.describe_vpcs(
        Filters=[
            {
                'Name': 'isDefault',
                'Values': ['true']
            }
        ]
    )
    try:
        vpcid = r['Vpcs'][0]['VpcId']
        r2 = ec2_conn_client.describe_subnets(
            Filters=[
                {
                    'Name': 'vpc-id',
                    'Values': [vpcid]
                }
            ]
        )
        if len(r2['Subnets']):
            return [s['SubnetId'] for s in r2['Subnets']]
        else:
            return None
    except IndexError:
        return None 

def get_tagged_instance_public_ip(key, value):
    ec2 = boto3.client('ec2')

    r = ec2.describe_instances(
        Filters=[
            {
                'Name': 'tag:{}'.format(key),
                'Values': [value]
            }
        ]
    )
    try:
        return [i['PublicDnsName'] for i in r['Reservations'][0]['Instances']]
    except IndexError:
        return None