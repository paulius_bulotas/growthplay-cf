import os
from troposphere import Parameter, Ref, Template, Join, AccountId, Region, Output, GetAtt
from troposphere.codepipeline import (
    Pipeline, Stages, Actions, ActionTypeId, OutputArtifacts,
    InputArtifacts, ArtifactStore
)
from troposphere.iam import PolicyType
from troposphere.iam import Role, InstanceProfile

from awacs.aws import Allow, Statement, Action, Principal, Policy
from awacs.sts import AssumeRole

from troposphere.elasticbeanstalk import (
    Application, ApplicationVersion, ConfigurationTemplate, Environment,
    SourceBundle, OptionSettings
)

t = Template()

SourceS3Bucket = t.add_parameter(Parameter(
    'SourceS3Bucket',
    Description='Source S3 bucket',
    Type='String'
))

spring_profile = t.add_parameter(Parameter(
    'SpringProfile',
    Type='String'
))

SourceS3ObjectKey = t.add_parameter(Parameter(
    'SourceS3ObjectKey',
    Description='Source object in S3 bucket',
    Type='String'
))

EBeanstalkApp = t.add_parameter(Parameter(
    'EBeanstalkApp',
    Description='Source object in S3 bucket',
    Type='String'
))

EBeanstalkEnv = t.add_parameter(Parameter(
    'EBeanstalkEnv',
    Description='Source object in S3 bucket',
    Type='String'
))

keyname = t.add_parameter(Parameter(
    'KeyName',
    Description='Name of an existing EC2 KeyPair to enable SSH access to AWS EB instance',
    Type='AWS::EC2::KeyPair::KeyName'
))

pipeline = t.add_resource(Pipeline(
    'LegendDaemonsBackendDevDeploy',
    RoleArn=Join(
        '',
        [
            'arn:aws:iam::',
            AccountId,
            ':role/AWS-CodePipeline-Service'
        ]
    ),
    ArtifactStore=ArtifactStore(
        Type='S3',
        Location=Join(
            '-',
            [
                'codepipeline',
                Region,
                AccountId
            ]
        )
    ),
    Stages=[
        Stages(
            Name='Source',
            Actions=[
                Actions(
                    Name='SourceAction',
                    ActionTypeId=ActionTypeId(
                        Category='Source',
                        Owner='AWS',
                        Version='1',
                        Provider='S3'
                    ),
                    OutputArtifacts=[
                        OutputArtifacts(
                            Name='MyApp'
                        )
                    ],
                    Configuration={
                        'PollForSourceChanges': True,
                        'S3Bucket': Ref(SourceS3Bucket),
                        'S3ObjectKey': Ref(SourceS3ObjectKey)
                    },
                    RunOrder='1'
                )
            ]
        ),
        Stages(
            Name='Staging',
            Actions=[
                Actions(
                    Name='StagingAction',
                    ActionTypeId=ActionTypeId(
                        Category='Deploy',
                        Owner='AWS',
                        Provider='ElasticBeanstalk',
                        Version='1'
                    ),
                    RunOrder='1',
                    Configuration={
                        'ApplicationName': Ref(EBeanstalkApp),
                        'EnvironmentName': Ref(EBeanstalkEnv)
                    },
                    InputArtifacts=[
                        InputArtifacts(
                            Name='MyApp'
                        )
                    ]
                )
            ]
        )
    ]
))

webserverrole = t.add_resource(Role(
    'WebServerRole',
    AssumeRolePolicyDocument=Policy(
        Statement=[
            Statement(
                Effect=Allow, Action=[AssumeRole],
                Principal=Principal(
                    'Service', 'ec2.amazonaws.com'
                )
            )
        ]
    ),
    Path='/'
))

t.add_resource(PolicyType(
    'WebServerRolePolicy',
    PolicyName='WebServerRole',
    PolicyDocument=Policy(
        Statement=[
            Statement(
                Effect=Allow,
                NotAction=Action('iam', '*'),
                Resource=['*']
            )
        ]
    ),
    Roles=[Ref(webserverrole)]
))

webserverinstanceprofile = t.add_resource(InstanceProfile(
    'WebServerInstanceProfile',
    Path='/',
    Roles=[Ref(webserverrole)]
))

app = t.add_resource(Application(
    'ElasticBeanstalkApp',
    Description='GrowthPlay Legend Daemons'
))

appver = t.add_resource(ApplicationVersion(
    'GrowthplayLegendDaemonsDevApp',
    Description='Version 1.0',
    ApplicationName=Ref(app),
    SourceBundle=SourceBundle(
        S3Bucket=Ref(SourceS3Bucket),
        S3Key=Ref(SourceS3ObjectKey)
    )
))

cfgtemplate = t.add_resource(ConfigurationTemplate(
    'ConfigurationTemplate',
    ApplicationName=Ref(app),
    Description='SSH access to app',
    SolutionStackName='64bit Amazon Linux 2018.03 v2.7.4 running Java 8',
    OptionSettings=[
        OptionSettings(
            Namespace='aws:autoscaling:launchconfiguration',
            OptionName='EC2KeyName',
            Value=Ref(keyname)
        ),
        OptionSettings(
            Namespace='aws:autoscaling:launchconfiguration',
            OptionName='IamInstanceProfile',
            Value=Ref(webserverinstanceprofile)
        ),
        OptionSettings(
            Namespace='aws:elasticbeanstalk:application:environment',
            OptionName='SPRING_PROFILES_ACTIVE',
            Value=Ref(spring_profile)
        ),
        OptionSettings(
            Namespace='aws:elasticbeanstalk:application:environment',
            OptionName='SERVER_PORT',
            Value='5000'
        )
    ]
))

appenv = t.add_resource(Environment(
    'ElasticBeanstalkEnvironment',
    Description='',
    ApplicationName=Ref(app),
    TemplateName=Ref(cfgtemplate),
    VersionLabel=Ref(appver)
))

t.add_output(
    Output(
        'URL',
        Description='URL of AWS Elastic Beanstalk Environment',
        Value=Join('', ['http://', GetAtt(appenv, 'EndpointURL')])
    )
)

print(t.to_json(sort_keys=True))

with open('{}.json'.format(os.path.splitext(os.path.basename(__file__))[0]), 'w') as f:
    f.write(t.to_json(sort_keys=True))
