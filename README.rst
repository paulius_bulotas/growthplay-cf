Growthplay Cloudformation
=========================

Welcome to Troposphere!
=======================
Troposphere (https://github.com/cloudtools/troposphere) is used write Python code which then could be transformed to CloudFormation.
This gives us more flexibility and allows to use loops, take values from environment (useful for some CI integration like Jenkins) etc.
If you want, you can save resulting CloudFormation code and continue from there modifying it. Most of the scripts currently do just that.
The only exception being rds.py, which shows how to invoke CloudFormation API through boto3 library.

Short how-to
============
Create virtual environment and install requirements:
::

    virtualenv .venv
    . .venv/{Scripts,bin}/activate # use according to your OS
    pip install -r requirements.txt
    python xxx.py

This prints CloudFormation output to console and updates/creates **xxx.json** file (file name is deduced from python code file name). That's where Python ends, although you could merge templates, do stack creation, waiting etc from Python itself.
Then use resulting json as parameter for aws cli cloudformation call, for example:
::

    aws cloudformation create-stack --stack-name ... --template-body file://yyy.json
        --parameters file://zzz.json --capabilities CAPABILITY_IAM

Where zzz.json format is:
::

    [
        {
            "ParameterKey": "parameter1",
            "ParameterValue": "value1"
        },
        {
            "ParameterKey": "parameter2",
            "ParameterValue": "value2"
        }
    ]

To find out what parameters you need, you can use:
::

    aws cloudformation validate-template --template-body  file://xxx.json


Wait for completion and get stack output
----------------------------------------
::

    aws cloudformation wait stack-create-complete --stack-name $STACK_NAME
    aws cloudformation describe-stacks --stack-name $STACK_NAME --query 'Stacks[0].Outputs[?OutputKey==`DatabaseHost`].OutputValue' --output text

Deleting stack
--------------
::

    aws cloudformation delete-stack --stack-name $STACK_NAME
    aws cloudformation wait stack-delete-complete --stack-name $STACK_NAME

Pipeline
========
For testing how everythings adds together I've created pipeline in Jenkins, which has the following jobs:

- RDS creation
- Database initialization
- Creation of backend artifacts
- Growthplay backend app and backend daemon launch through Elastic Beanstalk
- Creation of Growthplay frontend container image
- ECS launch of frontend container
- ...do something...
- Delete created CloudFormation stacks

All this (here) takes 25 minutes for creation, 5 minutes for deletion. Currently backend artifacts (zip, jar) have hardcoded DB connection details. You would want that to come through env to Elastic Beanstalk deployments. There is already example of passing SPRING_PROFILES_ACTIVE to EB.

RDS instance creation
---------------------
Parameters:

- AWS_DEFAULT_REGION
- DBUSER
- DBPASSWORD
- DBNAME
- DBPORT
- ALLOWCIDR

    List of CIDRs that will be allowed to connect to RDS instance, ECS frontend container

- PROPFILE

    Used for keeping state on CI server between jobs, could be avoided with proper pipeline

Build step - shell:
::

    rm -f ${PROPFILE}
    virtualenv .venv
    . .venv/bin/activate
    pip install -r requirements.txt
    STACK_NAME=$JOB_NAME-$BUILD_NUMBER
    python rds.py
    aws cloudformation wait stack-create-complete --stack-name $STACK_NAME
    DATABASEHOST=$(aws cloudformation describe-stacks --stack-name $STACK_NAME --query 'Stacks[0].Outputs[?OutputKey==`Host`].OutputValue' --output text)
    cat > ${PROPFILE} <<EOF
    RDS_STACK_NAME=${STACK_NAME}
    SPRING_DATASOURCE_URL=jdbc:mysql://${DATABASEHOST}:${DBPORT}/${DBNAME}\?useSSL=false&useUnicode=true&characterEncoding=UTF-8
    SPRING_DATASOURCE_USERNAME=${DBUSER}
    SPRING_DATASOURCE_PASSWORD=${DBPASSWORD}
    SPRING_DATASOURCE_TOMCAT_USERNAME=${DBUSER}
    SPRING_DATASOURCE_TOMCAT_PASSWORD=${DBPASSWORD}
    AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION}
    PROPFILE=${PROPFILE}
    ALLOWCIDR=${ALLOWCIDR}
    EOF

Triggering parametrized job with ${PROPFILE}.

Database initialization
-----------------------
Parameters:

- SPRING_DATASOURCE_URL
- SPRING_DATASOURCE_USERNAME
- SPRING_DATASOURCE_PASSWORD
- PROPFILE

Build step - Maven:
    Goals: spring-boot:run

Triggering parametrized job with ${PROPFILE}.

Creation of backend artifacts
-----------------------------
This modifies Spring profile awsdev and hardcodes DB connection info to jar's. Need to pass this through environment to Elastic Beanstalk launch job.
Note, that other parameters are not changed for CloudFormation env so something will definitely fail.
And there is chicken/egg problem with hardcoded frontend address (as it's not known at this point), unless CloudFormation would hook to some specific DNS name that you can hardcode.

Parameters:

- PROPFILE
- S3BUCKET -> growthplay-legend-backend2
- SPRING_DATASOURCE_URL
- SPRING_DATASOURCE_TOMCAT_USERNAME
- SPRING_DATASOURCE_TOMCAT_PASSWORD
- AWS_DEFAULT_REGION

Pre step:
    Shell:
    ::

        echo -e "spring:\r\n  datasource:\r\n    url: ${SPRING_DATASOURCE_URL}\r\n    tomcat:\r\n      username: ${SPRING_DATASOURCE_TOMCAT_USERNAME}\r\n      password: ${SPRING_DATASOURCE_TOMCAT_PASSWORD}" >> ./backend/legend-daemons/src/main/resources/application-awsdev.yml
        echo -e "spring:\r\n  datasource:\r\n    url: ${SPRING_DATASOURCE_URL}\r\n    tomcat:\r\n      username: ${SPRING_DATASOURCE_TOMCAT_USERNAME}\r\n      password: ${SPRING_DATASOURCE_TOMCAT_PASSWORD}\r\nlegend:\r\n  security:\r\n    client:\r\n      cognito:\r\n        client-login-url: https://gp-legend.devbstaging.com/login-client/\r\n  email:\r\n    survey-url: https://gp-legend.devbstaging.com/assessment/" >> ./backend/legend-app/src/main/resources/application-awsdev.yml
        echo -e "spring:\r\n  datasource:\r\n    url: ${SPRING_DATASOURCE_URL}\r\n    username: ${SPRING_DATASOURCE_TOMCAT_USERNAME}\r\n    password: ${SPRING_DATASOURCE_TOMCAT_PASSWORD}" > ./backend/legend-database-generation/src/main/resources/application-awsdev.yml

Build step - Maven:
    Root POM: backend/pom.xml
    Goals: clean package

Post step - shell:
::

    cp backend/legend-daemons/target/legend-daemons-*.jar .
    cp -ar tools/.ebextensions .
    zip -r legend-daemons.zip legend-daemons-*.jar .ebextensions
    aws s3 cp --no-progress legend-daemons.zip s3://${S3BUCKET}/
    echo "S3DAEMONKEY=legend-daemons.zip" >> ${PROPFILE}

Post step - shell:
::

    aws s3 cp --no-progress backend/legend-app/target/legend-app-*-SNAPSHOT.jar s3://${S3BUCKET}/
    FILE=$(basename `ls backend/legend-app/target/legend-app-*-SNAPSHOT.jar`)
    echo "S3APPKEY=${FILE}" >> ${PROPFILE}

Triggering parametrized job with ${PROPFILE}.

Growthplay backend app and backend daemon launch through Elastic Beanstalk
--------------------------------------------------------------------------
Parameters:

- S3APPKEY
- S3DAEMONKEY
- PROPFILE
- AWS_DEFAULT_REGION

Build steps:
    Shell:
    ::

        virtualenv .venv
        . .venv/bin/activate
        pip install -U pip setuptools
        pip install -r requirements.txt
        python eb.py
        test -f eb.json

    Shell:
    ::

        JSON_PARAMS=eb-daemon-params.json
        test -f ${JSON_PARAMS}
        sed -i -e "s/S3DAEMONKEY/${S3DAEMONKEY}/" ${JSON_PARAMS}
        STACK_NAME="${JOB_NAME}-DAEMON-${BUILD_NUMBER}"
        aws cloudformation create-stack --stack-name ${STACK_NAME} --template-body file://eb.json --parameters file://${JSON_PARAMS} --capabilities CAPABILITY_IAM
        aws cloudformation wait stack-create-complete --stack-name $STACK_NAME
        echo "EB_DAEMON_STACK_NAME=${STACK_NAME}" >> ${PROPFILE}

    Shell:
    ::

        JSON_PARAMS=eb-app-params.json
        test -f ${JSON_PARAMS}
        sed -i -e "s/S3APPKEY/${S3DAEMONKEY}/" ${JSON_PARAMS}
        STACK_NAME="${JOB_NAME}-APP-${BUILD_NUMBER}"
        aws cloudformation create-stack --stack-name ${STACK_NAME} --template-body file://eb.json --parameters file://${JSON_PARAMS} --capabilities CAPABILITY_IAM
        aws cloudformation wait stack-create-complete --stack-name $STACK_NAME
        echo "EB_APP_STACK_NAME=${STACK_NAME}" >> ${PROPFILE}
        EB_APP_URL=$(aws cloudformation describe-stacks --stack-name $STACK_NAME --query 'Stacks[0].Outputs[?OutputKey==`URL`].OutputValue' --output text)
        echo "EB_APP_URL=${EB_APP_URL}" >> ${PROPFILE}

Triggering parametrized job with ${PROPFILE}.

Creation of Growthplay frontend container image
-----------------------------------------------
Parameters:

- AWS_DEFAULT_REGION
- S3BUCKET -> legend-ssl
- PROPFILE

Build steps:
    Shell:
    ::

        npm install --prefix frontend
        npm run test --prefix frontend
        npm run build --prefix frontend

    Shell:
    ::

        aws s3 cp s3://${S3BUCKET}/ ./tools/docker-nginx/ssl/ --recursive

    Shell:
    ::

        docker build -t growthplay-legend:v2 -f tools/docker-nginx/Dockerfile-w-params.init .
        eval $(aws ecr get-login --no-include-email)
        docker tag growthplay-legend:v2 755225559539.dkr.ecr.us-east-2.amazonaws.com/growthplay-legend:v2
        docker push 755225559539.dkr.ecr.us-east-2.amazonaws.com/growthplay-legend:v2

Triggering parametrized job with ${PROPFILE}.

ECS launch of frontend container
--------------------------------
This required Dockerfile modification to take backend app url through environment, as CloudFormation generates "random" url each time.
SSL certs usage should also be automated.

Parameters:

- EB_APP_URL
- ALLOWCIDR
- AWS_DEFAULT_REGION
- PROPFILE

Build steps:
    Shell:
    ::

        virtualenv .venv
        . .venv/bin/activate
        pip install -U pip setuptools
        pip install -r requirements.txt
        python ecs.py
        test -f ecs.json
        STACK_NAME="${JOB_NAME}-${BUILD_NUMBER}"
        JSON_PARAMS=ecs-params.json
        aws cloudformation create-stack --stack-name ${STACK_NAME} --template-body file://ecs.json --parameters file://${JSON_PARAMS} --capabilities CAPABILITY_IAM
        aws cloudformation wait stack-create-complete --stack-name $STACK_NAME
        echo "ECS_STACK_NAME=$STACK_NAME" >> ${PROPFILE}
        # this assumes that STACK_NAME is defined
        FRONTEND_URL=$(python ecs_url.py)
        echo "FRONTEND_URL=${FRONTEND_URL}" >> ${PROPFILE}

Cleanup job
-----------
Parameters:

- PROPFILE
- AWS_DEFAULT_REGION

Build steps:
    Shell:
    ::
    
        test -f ${PROPFILE}
        for stack in `grep STACK_NAME /var/tmp/growthplay.properties | awk -F= '{ print $NF; }'`; do
            aws cloudformation delete-stack --stack-name ${stack}
        done
        for stack in `grep STACK_NAME /var/tmp/growthplay.properties | awk -F= '{ print $NF; }'`; do
            aws cloudformation wait stack-delete-complete --stack-name ${stack}
        done
        rm -f ${PROPFILE}