# used troposphere examples and
# https://dzone.com/articles/automating-ecs-provisioning-in-cloudformation-part

import os
import utils

from troposphere.ecs import (
    Cluster, Service, TaskDefinition, ContainerDefinition, PortMapping, Environment, LoadBalancer as EcsLoadBalancer
)

from troposphere.elasticloadbalancing import LoadBalancer, Listener

from troposphere import (
    Parameter, Ref, Template, ec2, FindInMap, Region, AccountId,
    Join, Base64, GetAZs, GetAtt, StackName
)

from troposphere.ec2 import (
    SecurityGroup, SecurityGroupRule
)

from troposphere.iam import InstanceProfile, Role, PolicyType, Policy
from troposphere.autoscaling import LaunchConfiguration, Metadata, AutoScalingGroup, Tag
from troposphere.cloudformation import Init, InitConfig


t = Template()

"""
  TODO:
  - In theory we need to pass VPC name as parameter or use default VPC,
  and get it's subnets programatically using boto3 thus shrinking parameter list
  - Add Security Group through CloudFormation instead of using existing
  one (if you would like to spin in another region etc)
"""
AppName = t.add_parameter(Parameter(
    'ApplicationName',
    Type='String',
    AllowedPattern='[a-zA-Z][a-zA-Z0-9]*'
))

keyname = t.add_parameter(Parameter(
    'KeyName',
    Type='String',
    AllowedPattern='[a-zA-Z][a-zA-Z0-9]*'
))

ECSRepoName = t.add_parameter(Parameter(
    'ECSRepoName',
    Type='String'
))

ImageTag = t.add_parameter(Parameter(
    'ImageTag',
    Type='String'
))

cidrListStr = os.environ.get('ALLOWCIDR', '')
cidrList = cidrListStr.strip().split(',')
cidrList.append(utils.get_default_vpc_cidr())
if len(cidrList) == 0:
    print("cidr list for RDS access security group is missing, aborting")
    sys.exit(1)

sgiList = []
for c in set(cidrList):
    sgiList.append(
        SecurityGroupRule(
            IpProtocol='tcp',
            FromPort='0',
            ToPort='65535',
            CidrIp=c
        )
    )

sg = t.add_resource(SecurityGroup(
    'ECSSecurityGroup',
    GroupDescription='ECS Security group',
    SecurityGroupIngress=sgiList
))

ecscluster = t.add_resource(Cluster(
    'ECSCluster'
))

# Taken from https://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs-optimized_AMI.html
t.add_mapping('AMIRegionMap',
    {
        "us-east-1":{
            "ID":"ami-0254e5972ebcd132c"
        },
        "us-east-2":{
            "ID":"ami-0a0d2004b44b9287c"
        },
        "us-west-1":{
            "ID":"ami-0de5608ca20c07aa2"
        },
        "us-west-2":{
            "ID":"ami-093381d21a4fc38d1"
        },
        "eu-west-1":{
            "ID":"ami-0dbcd2533bc72c3f6"
        },
        "eu-west-2":{
            "ID":"ami-005307409c5f6e76c"
        },
        "eu-west-3":{
            "ID":"ami-024c0b7d07abc6526"
        },
        "eu-central-1":{
            "ID":"ami-03804565a6baf6d30"
        }
    }
)

EcsClusterRole = t.add_resource(Role(
    'EcsClusterRole',
    Path='/',
    ManagedPolicyArns=[
        'arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM',
        'arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly'
    ],
    AssumeRolePolicyDocument={
        'Version': '2012-10-17',
        'Statement': [
            {
                'Action': 'sts:AssumeRole',
                'Principal': {'Service': 'ec2.amazonaws.com'},
                'Effect': 'Allow'
            }
        ]
    }
))

PolicyEcs = t.add_resource(PolicyType(
    'PolicyEcs',
    PolicyName='EcsPolicy',
    PolicyDocument={
        'Version': '2012-10-17',
        'Statement': [
            {
                'Action': [
                    'ecs:CreateCluster',
                    'ecs:RegisterContainerInstance',
                    'ecs:DeregisterContainerInstance',
                    'ecs:DiscoverPollEndpoint',
                    'ecs:Submit*',
                    'ecs:Poll',
                    'ecs:StartTelemetrySession'
                ],
                'Resource': '*',
                'Effect': 'Allow'
            }
        ]
    },
    Roles=[Ref(EcsClusterRole)],))

container = ContainerDefinition(
    Name=Ref(AppName),
    Cpu='10',
    Memory='300',
    Image=Join('', [
        AccountId,
        '.dkr.ecr.',
        Region,
        '.amazonaws.com/',
        Ref(ECSRepoName),
        ':',
        Ref(ImageTag)
    ]),
    Environment=[
        Environment(
            Name='BackendURL',
            Value=os.environ['EB_APP_URL']
        )
    ],
    Essential=True,
    PortMappings=[
        PortMapping(HostPort=80, ContainerPort=80),
        PortMapping(HostPort=443, ContainerPort=443)
    ]
)

task = t.add_resource(TaskDefinition(
    'TaskDefinition',
    RequiresCompatibilities=['EC2'],
    ContainerDefinitions=[container]
))

ecsservice = t.add_resource(Service(
    'AppService',
    Cluster=Ref(ecscluster),
    DesiredCount=1,
    LaunchType='EC2',
    TaskDefinition=Ref(task)
))

EC2InstanceProfile = t.add_resource(InstanceProfile(
    'EC2InstanceProfile',
    Path='/',
    Roles=[Ref(EcsClusterRole)]
))

ContainerInstances = t.add_resource(LaunchConfiguration(
    'ContainerInstances',
    Metadata=Metadata(
        Init({
            'config': InitConfig(
                commands={
                    '01_add_instance_to_cluster': {
                        'command': Join(
                            '',
                            ['#!/bin/bash\n',
                             'echo ECS_CLUSTER=',
                             Ref(ecscluster),
                             ' >> /etc/ecs/ecs.config']
                        )
                    }
                }
            )
        })),
        UserData=Base64(Join(
                '',
                [
                    '#!/bin/bash -xe\n',
                    'yum install -y aws-cfn-bootstrap\n',
                    '/opt/aws/bin/cfn-init -v ',
                    '         --stack ',
                    Ref('AWS::StackName'),
                    '         --resource ContainerInstances ',
                    '         --region ',
                    Region,
                    '\n',
                    '/opt/aws/bin/cfn-signal -e $? ',
                    '         --stack ',
                    Ref('AWS::StackName'),
                    '         --resource ECSAutoScalingGroup ',
                    '         --region ',
                    Region,
                    '\n'
                ]
            )
        ),
    ImageId=FindInMap('AMIRegionMap', Region, 'ID'),
    KeyName=Ref(keyname),
    SecurityGroups=[GetAtt(sg, 'GroupId')],
    IamInstanceProfile=Ref(EC2InstanceProfile),
    InstanceType='t2.small',
    AssociatePublicIpAddress='true'
))

EcsAutoScalingGroup = t.add_resource(AutoScalingGroup(
    'ECSAutoScalingGroup',
    DesiredCapacity='1',
    MinSize='1',
    MaxSize='1',
    VPCZoneIdentifier=utils.get_default_vpc_subnets(),
    AvailabilityZones=GetAZs(Region),
    LaunchConfigurationName=Ref(ContainerInstances),
    Tags=[
        Tag('Stack', StackName, True)
    ]
))


print(t.to_json(sort_keys=True))

with open('{}.json'.format(os.path.splitext(os.path.basename(__file__))[0]), 'w') as f:
    f.write(t.to_json(sort_keys=True))
